package no.noroff.accelerate.moviecharacters.controllers;

import no.noroff.accelerate.moviecharacters.dtos.FranchiseDTO;
import no.noroff.accelerate.moviecharacters.mappers.FranchiseMapper;
import no.noroff.accelerate.moviecharacters.models.Franchise;
import no.noroff.accelerate.moviecharacters.services.franchise.FranchiseService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path="api/v1/franchises")
public class FranchiseController {
    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;

    public FranchiseController(FranchiseService franchiseService, FranchiseMapper franchiseMapper){
        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
    }

    //GET
    @Operation(summary = "Gets all franchises")
    @GetMapping // api/v1/franchises
    public ResponseEntity getAll(){
        Collection<FranchiseDTO> franchiseDTO = franchiseMapper.franchisesToFranchisesDTO(
                franchiseService.findAll()
        );
        return ResponseEntity.ok(franchiseDTO);
    }

    @Operation(summary = "Gets franchise by id")
    @GetMapping("/{id}") // api/v1/franchises/id
    public ResponseEntity getById(@PathVariable int id) {
        FranchiseDTO franchiseDTO = franchiseMapper.franchiseToFranchiseDTO(
                franchiseService.findById(id)
        );
        return ResponseEntity.ok(franchiseDTO);
    }

    @Operation(summary= "Gets franchises by name")
    @GetMapping("/search") // api/v1/franchises/search
    public ResponseEntity searchByName(@RequestParam String name){
        Collection<FranchiseDTO> franchiseDTOS = franchiseMapper.franchisesToFranchisesDTO(
                franchiseService.findAllByName(name)
        );
        return ResponseEntity.ok(franchiseDTOS);
    }

    //POST
    @Operation(summary = "Adds a new franchise")
    @PostMapping // api/v1/franchises
    public ResponseEntity add(@RequestBody FranchiseDTO franchiseDTO){
        Franchise fran = franchiseService.add(
                franchiseMapper.franchiseDTOtoFranchise(franchiseDTO)
        );
        URI location = URI.create("franchises/" + fran.getId());
        return ResponseEntity.created(location).build();
    }

    //PUT
    @Operation(summary = "Update franchise by id")
    @PutMapping("{id}") // /api/v1/franchises/id
    public ResponseEntity update(@RequestBody FranchiseDTO franchiseDTO, @PathVariable int id) {
        // Validates if body is correct
        if(id != franchiseDTO.getId())
            return ResponseEntity.badRequest().build();
        franchiseService.update(
                franchiseMapper.franchiseDTOtoFranchise(franchiseDTO)
        );
        return ResponseEntity.noContent().build();
    }

    //DELETE
    @Operation(summary = "Delete franchise by Id")
    @DeleteMapping("/{id}") // api/v1/franchises/id
    public ResponseEntity delete(@PathVariable int id){
        franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}