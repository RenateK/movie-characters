package no.noroff.accelerate.moviecharacters.services.franchise;

import no.noroff.accelerate.moviecharacters.exceptions.FranchiseNotFoundException;
import no.noroff.accelerate.moviecharacters.models.Franchise;
import no.noroff.accelerate.moviecharacters.repositories.FranchiseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service("FranchiseServiceImplementation")
public class FranchiseServiceImpl implements FranchiseService{
    private final FranchiseRepository franchiseRepository;
    private final Logger logger = LoggerFactory.getLogger(FranchiseServiceImpl.class);

    public FranchiseServiceImpl(FranchiseRepository franchiseRepository){
        this.franchiseRepository = franchiseRepository;
    }

    @Override
    public Franchise findById(Integer id) {
        return franchiseRepository.findById(id).orElseThrow(() ->
                new FranchiseNotFoundException(id));
    }

    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }

    @Override
    public Franchise add(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public Franchise update(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        if(franchiseRepository.existsById(id)){
            Franchise franchise = franchiseRepository.findById(id).get();
            franchise.getMovies().forEach(m -> m.setFranchise(null));
            franchiseRepository.delete(franchise);
        }else {
            logger.warn("Franchise with id " + id + " is not found");
        }

    }
    @Override
    public Collection<Franchise> findAllByName(String name) {
        return franchiseRepository.findAllByName(name);
    }
    @Override
    public boolean exists(Integer id) {
        return franchiseRepository.existsById(id);
    }
}
