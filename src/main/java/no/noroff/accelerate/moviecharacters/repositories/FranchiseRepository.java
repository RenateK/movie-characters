package no.noroff.accelerate.moviecharacters.repositories;

import no.noroff.accelerate.moviecharacters.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Integer> {
    Collection<Franchise> findAllByName(String name);
}