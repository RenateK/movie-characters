package no.noroff.accelerate.moviecharacters.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 60, nullable = false)
    private String name;
    @Column(length = 60, nullable = true)
    private String alias;
    @Column(length = 30, nullable = false)
    private String gender;
    @Column(length = 300, nullable = true)
    private String imageUrl;

    @ManyToMany(mappedBy = "characters")
    private Set<Movie> movies;
}