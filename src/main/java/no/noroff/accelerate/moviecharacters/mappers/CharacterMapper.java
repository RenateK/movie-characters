package no.noroff.accelerate.moviecharacters.mappers;

import no.noroff.accelerate.moviecharacters.dtos.CharacterDTO;
import no.noroff.accelerate.moviecharacters.models.Character;
import no.noroff.accelerate.moviecharacters.models.Movie;
import no.noroff.accelerate.moviecharacters.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class CharacterMapper {
    @Autowired
    protected MovieService movieService;
    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
    public abstract CharacterDTO characterToCharacterDTO(Character character);
    public abstract Collection<CharacterDTO> charactersToCharactersDTO(Collection<Character> characters);

    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdToMovie")
    public abstract Character characterDTOtoCharacter(CharacterDTO characterDTO);

    // Custom mappings
    @Named("movieIdToMovie")
    Movie mapIdToMovie(int id) {
        return movieService.findById(id);
    }

    @Named("moviesToIds")
    Set<Integer> map(Set<Movie> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(m -> m.getId()).collect(Collectors.toSet());
    }
}
