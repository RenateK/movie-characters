package no.noroff.accelerate.moviecharacters.services.franchise;

import no.noroff.accelerate.moviecharacters.models.Franchise;
import no.noroff.accelerate.moviecharacters.services.CrudService;
import java.util.Collection;

public interface FranchiseService extends CrudService<Franchise, Integer> {
    Collection<Franchise> findAllByName(String name);
}
