# About this project
Group members are Renate Karlsen og Günel Shirinova. 
In this project we are created a Web API for PostgreSQL database, using Hibernate and Spring.

## Dependencies
* Spring Web
* Spring Data JPA
* PostgreSQL
* Lombok
* Mapstruct
* OpenAPI (swagger)

The assignment are seperated in two parts: Appendix A and Appendix B.

### Appendix A
In Appendix A we created a PostgreSQL database with PgAdmin. 
The entities are:
* Character
* Movie
* Franchise

We have three tables: Character, Movie and Franchise. Character og Movies tables have many to many relation between
each other. In Character class we used "Set" for movies. In Movie class we have both many to one and many to many 
relations between tables. Franchise table has one to many relation between movie table. 

PostgreSql is used for database solution. Repository classes extends JpaRepository. The repository includes methods to 
work with CRUD operations. CrudService is a custom repository class and it is implemented by all service classes. 
There are made custom exception classes to handle errors. 

### Appendix B
In Appendix B we created a Web API with Spring Web. In Controllers classes we created many methods to handle 
APIs. We have following methods: 
- getAll() - uses GET mapping and returns all characters/movies/franchises 
- getById(@PathVariable int id) - uses GET mapping and returns the specific character/movie/franchise by given id
- searchByName(@RequestParam String name) - uses GET mapping and returns the specific character/movie/franchise given by the title/name
- add(@RequestBody CharacterDTO characterDTO) - uses POST mapping and adds a new character/movie/franchise to the database
- update(@RequestBody CharacterDTO characterDTO, @PathVariable int id) - uses PUT mapping and updates a movie/character/franchise by given id
- delete(@PathVariable int id) - uses DELETE mapping and deletes a specific character/movie/franchise by the given id



