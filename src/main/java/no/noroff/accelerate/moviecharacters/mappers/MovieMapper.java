package no.noroff.accelerate.moviecharacters.mappers;

import no.noroff.accelerate.moviecharacters.dtos.MovieDTO;
import no.noroff.accelerate.moviecharacters.models.Character;
import no.noroff.accelerate.moviecharacters.models.Franchise;
import no.noroff.accelerate.moviecharacters.models.Movie;
import no.noroff.accelerate.moviecharacters.services.character.CharacterService;
import no.noroff.accelerate.moviecharacters.services.franchise.FranchiseService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;


@Mapper(componentModel = "spring")
public abstract class MovieMapper {
    @Autowired
    protected FranchiseService franchiseService;
    @Autowired
    protected CharacterService characterService;

    @Mapping(target = "franchise", source = "franchise.id")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "charactersToIds")
    public abstract MovieDTO movieToMovieDto(Movie movie);


    public abstract Collection<MovieDTO> movieToMovieDto(Collection<Movie> movies);

    @Mapping(target = "franchise", source = "franchise", qualifiedByName = "franchiseIdsToFranchise")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "characterIdsToCharacters")
    public abstract Movie movieDtoToMovie(MovieDTO movieDTO);

    @Named("characterIdsToCharacters")
    Set<Character> mapIdToCharacter(Set<Integer> id){
        if(id == null){
            return null;
        }
        return id.stream()
                .map(i -> characterService.findById(i))
                .collect(Collectors.toSet());

    }

    @Named("franchiseIdsToFranchise")
    Franchise mapIdToFranchise(int id){
        return franchiseService.findById(id);
    }



    @Named("charactersToIds")
     Set<Integer> map(Set<Character> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }
}
