package no.noroff.accelerate.moviecharacters.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class CharacterNotFound extends RuntimeException{
    public CharacterNotFound(int id){
        super("Character with id " + id + " is not found!");
    }
}
