package no.noroff.accelerate.moviecharacters.repositories;

import no.noroff.accelerate.moviecharacters.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface CharacterRepository extends JpaRepository<Character, Integer> {


    Collection<Character> findAllByName(String name);

    //Updating characters in a movie
    @Modifying
    @Query(value = "update character c set id = ?2 where c.id = ?1", nativeQuery = true)
    int updateCharactersMoviesById(int char_id, int movie_id);

}
