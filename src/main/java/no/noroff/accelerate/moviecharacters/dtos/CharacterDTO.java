package no.noroff.accelerate.moviecharacters.dtos;

import lombok.Getter;
import lombok.Setter;
import java.util.Set;

@Getter
@Setter
public class CharacterDTO {
    private int id;
    private String name;
    private String alias;
    private String gender;
    private String imageUrl;
    private Set<Integer> movies;
}
