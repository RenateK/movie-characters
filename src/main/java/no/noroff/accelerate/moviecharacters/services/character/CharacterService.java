package no.noroff.accelerate.moviecharacters.services.character;

import no.noroff.accelerate.moviecharacters.models.Character;
import no.noroff.accelerate.moviecharacters.services.CrudService;
import java.util.Collection;

public interface CharacterService extends CrudService<Character, Integer> {
    Collection<Character> findAllByName(String name);
}
