package no.noroff.accelerate.moviecharacters.controllers;

import no.noroff.accelerate.moviecharacters.dtos.CharacterDTO;
import no.noroff.accelerate.moviecharacters.mappers.CharacterMapper;
import no.noroff.accelerate.moviecharacters.models.Character;
import no.noroff.accelerate.moviecharacters.services.character.CharacterService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path="api/v1/characters")
public class CharacterController {
    private final CharacterService characterService;
    private final CharacterMapper characterMapper;

    public CharacterController(CharacterService characterService, CharacterMapper characterMapper){
        this.characterService = characterService;
        this.characterMapper = characterMapper;
    }

    //GET
    @Operation(summary = "Gets all characters")
    @GetMapping // api/v1/characters
    public ResponseEntity getAll(){
        Collection<CharacterDTO> characters = characterMapper.charactersToCharactersDTO(
                characterService.findAll()
        );
        return ResponseEntity.ok(characters);
    }

    @Operation(summary = "Gets character by id")
    @GetMapping("{id}") // api/v1/characters/id
    public ResponseEntity getById(@PathVariable int id) {
        CharacterDTO characterDTO = characterMapper.characterToCharacterDTO(
                characterService.findById(id)
        );
        return ResponseEntity.ok(characterDTO);
    }

    @Operation(summary = "Gets characters by name")
    @GetMapping("/search") // api/v1/characters/search
    public ResponseEntity searchByName(@RequestParam String name){
        Collection<CharacterDTO> characterDTOS = characterMapper.charactersToCharactersDTO(
                characterService.findAllByName(name)
        );
        return ResponseEntity.ok(characterDTOS);
    }

    //POST
    @Operation(summary = "Adds a new character")
    @PostMapping // api/v1/characters
    public ResponseEntity add(@RequestBody CharacterDTO characterDTO){
        Character charac = characterService.add(
                characterMapper.characterDTOtoCharacter(characterDTO)
        );
        URI location = URI.create("characters/" + charac.getId());
        return ResponseEntity.created(location).build();
    }

    //PUT
    @Operation(summary = "Update character by id")
    @PutMapping("{id}") // /api/v1/characters/id
    public ResponseEntity update(@RequestBody CharacterDTO characterDTO, @PathVariable int id) {
        // Validates if body is correct
        if(id != characterDTO.getId())
            return ResponseEntity.badRequest().build();
        characterService.update(
                characterMapper.characterDTOtoCharacter(characterDTO)
        );
        return ResponseEntity.noContent().build();
    }

    //DELETE
    @Operation(summary = "Delete character by id")
    @DeleteMapping("/{id}") // api/v1/characters/id
    public ResponseEntity delete(@PathVariable int id){
        characterService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}