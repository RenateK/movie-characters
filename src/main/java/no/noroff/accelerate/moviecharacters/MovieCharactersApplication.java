package no.noroff.accelerate.moviecharacters;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
public class MovieCharactersApplication {

    public static void main(String[] args) {
        SpringApplication.run(MovieCharactersApplication.class, args);
    }

}
