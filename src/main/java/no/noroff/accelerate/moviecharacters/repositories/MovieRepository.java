package no.noroff.accelerate.moviecharacters.repositories;

import no.noroff.accelerate.moviecharacters.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {


    Collection<Movie> findAllByTitle(String title);
}
