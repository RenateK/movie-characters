package no.noroff.accelerate.moviecharacters.models;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 60, nullable = false)
    private String name;
    @Column(length = 500, nullable = true)
    private String description;

    @OneToMany(mappedBy = "franchise")
    private Set<Movie> movies;
}
