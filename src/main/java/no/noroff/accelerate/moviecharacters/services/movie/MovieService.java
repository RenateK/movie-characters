package no.noroff.accelerate.moviecharacters.services.movie;

import no.noroff.accelerate.moviecharacters.models.Movie;
import no.noroff.accelerate.moviecharacters.services.CrudService;

import java.util.Collection;

public interface MovieService extends CrudService<Movie, Integer> {

    Collection<Movie> findAllByTitle(String title);
}
