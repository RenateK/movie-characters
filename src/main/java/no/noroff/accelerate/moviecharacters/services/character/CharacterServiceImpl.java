package no.noroff.accelerate.moviecharacters.services.character;

import no.noroff.accelerate.moviecharacters.exceptions.CharacterNotFound;
import no.noroff.accelerate.moviecharacters.models.Character;
import no.noroff.accelerate.moviecharacters.repositories.CharacterRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;


@Service("CharacterServiceImplementation")
public class CharacterServiceImpl implements CharacterService {
    private final CharacterRepository characterRepository;
    private final Logger logger = LoggerFactory.getLogger(CharacterServiceImpl.class);

    public CharacterServiceImpl(CharacterRepository characterRepository){
        this.characterRepository = characterRepository;
    }

    @Override
    public Character findById(Integer id) {
       return characterRepository.findById(id).orElseThrow(()
               -> new CharacterNotFound(id));
    }

    @Override
    public Collection<Character> findAll() {
        return characterRepository.findAll();
    }

    @Override
    public Character add(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    public Character update(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        if(characterRepository.existsById(id)){
            Character character = characterRepository.findById(id).get();
            character.getMovies().forEach(m -> m.setCharacters(null));
            characterRepository.delete(character);
        }else {
            logger.warn("No character with id " + id);
        }


    }
    @Override
    public Collection<Character> findAllByName(String name) {
        return characterRepository.findAllByName(name);
    }
    @Override
    public boolean exists(Integer id) {
        return characterRepository.existsById(id);
    }
}


