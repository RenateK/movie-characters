package no.noroff.accelerate.moviecharacters.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 160, nullable = false)
    private String title;
    @Column(length = 300, nullable = false)
    private String genre;
    @Column(length = 4, nullable = false)
    private int releaseYear;
    @Column(length = 60, nullable = true)
    private String director;
    @Column(length = 300, nullable = true)
    private String imageUrl;
    @Column(length = 300, nullable = true)
    private String trailerLink;

    @ManyToOne
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "movie_character",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "character_id")}
    )
    private Set<Character> characters;


}