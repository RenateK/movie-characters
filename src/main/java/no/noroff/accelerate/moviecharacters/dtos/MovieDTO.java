package no.noroff.accelerate.moviecharacters.dtos;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import no.noroff.accelerate.moviecharacters.models.Character;
import no.noroff.accelerate.moviecharacters.models.Franchise;

import java.util.Set;

@Getter
@Setter
public class MovieDTO {
    private int id;
    private String title;
    private String genre;
    private int releaseYear;
    private String director;
    private String imageUrl;
    private String trailerLink;
    private int franchise;
    private Set<Integer> characters;

}
