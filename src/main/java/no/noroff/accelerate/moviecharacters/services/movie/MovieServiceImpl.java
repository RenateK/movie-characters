package no.noroff.accelerate.moviecharacters.services.movie;

import no.noroff.accelerate.moviecharacters.exceptions.MovieNotFoundException;
import no.noroff.accelerate.moviecharacters.models.Movie;
import no.noroff.accelerate.moviecharacters.repositories.MovieRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.util.Collection;

@Service("MovieServiceImplementation")
public class MovieServiceImpl implements MovieService{
    private final MovieRepository movieRepository;
    private final Logger logger = LoggerFactory.getLogger(MovieServiceImpl.class);

    public MovieServiceImpl(MovieRepository movieRepository){
        this.movieRepository = movieRepository;
    }

    @Override
    public Movie findById(Integer id) {
        return movieRepository.findById(id).orElseThrow(() -> new MovieNotFoundException(id));
    }

    @Override
    public Collection<Movie> findAll() {
       return movieRepository.findAll();
    }

    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public Movie update(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        if(movieRepository.existsById(id)){
            Movie movie = movieRepository.findById(id).get();
            movie.getCharacters().forEach(c -> c.setMovies(null));
            movieRepository.delete(movie);
        }else {
            logger.warn("No movies exist with id " + id);
        }
    }
    @Override
    public boolean exists(Integer id) {
        return movieRepository.existsById(id);
    }

    @Override
    public Collection<Movie> findAllByTitle(String title) {
        return movieRepository.findAllByTitle(title);
    }
}
