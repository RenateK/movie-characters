package no.noroff.accelerate.moviecharacters.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.accelerate.moviecharacters.dtos.MovieDTO;
import no.noroff.accelerate.moviecharacters.mappers.MovieMapper;
import no.noroff.accelerate.moviecharacters.models.Movie;
import no.noroff.accelerate.moviecharacters.services.movie.MovieService;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1")
public class MovieController {
    private final MovieService movieService;

    private final MovieMapper movieMapper;

    public MovieController(MovieService movieService, MovieMapper movieMapper) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;
    }

    @GetMapping(path = "/movies")
    public ResponseEntity getAll(){

        Collection<MovieDTO> movieDTOS = movieMapper.movieToMovieDto(
                movieService.findAll()
        );
        return ResponseEntity.ok(movieDTOS);
    }

    @Operation(summary = "Gets a specific movie")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Successfully gets the movie",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class)
                    )),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorAttributeOptions.class)

                    )),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with supplied ID",
                    content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiResponse.class)

                    ))
    })
    @GetMapping(path = "/movies/{id}")
    public ResponseEntity getAllById(@PathVariable Integer id){

        MovieDTO movie = movieMapper.movieToMovieDto(
                movieService.findById(id)
        );
        return ResponseEntity.ok(movie);
    }
    @DeleteMapping("/movies/{id}")
    public ResponseEntity delete(@PathVariable int id){
        movieService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/movies/search")
    public ResponseEntity findByName(@RequestParam String title){
        Collection<MovieDTO> movieDTO = movieMapper.movieToMovieDto(
                movieService.findAllByTitle(title)
        );
       return ResponseEntity.ok(movieDTO);

    }

    @PutMapping("/movies/{id}")
    public ResponseEntity update(@RequestBody MovieDTO movieDTO, @PathVariable int id){
        if(id != movieDTO.getId())
            return ResponseEntity.badRequest().build();

        movieService.update(
                movieMapper.movieDtoToMovie(movieDTO)
        );
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "adds a new movie to the database")
    @PostMapping(path = "/movies")
    public ResponseEntity add(@RequestBody MovieDTO movie){
        Movie movie1 = movieService.add(movieMapper.movieDtoToMovie(movie));
        URI location = URI.create("movies/" + movie1.getId());
        return ResponseEntity.created(location).build();
    }
}
