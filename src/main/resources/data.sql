INSERT INTO franchise (name, description)
VALUES ('Shrek', 'The Shrek franchise is characterized by its fantastical mixed fairytale setting, as well as its blend of colorful, kid-friendly designs and slapstick with adult-oriented humor, satire and pop-culture references.');

INSERT INTO movie (title, genre, release_year, director, image_url, trailer_link, franchise_id)
VALUES ('Shrek', 'Comedy, Adventure, Animation', 2001, 'Andrew Adamson', 'https://en.wikipedia.org/wiki/Shrek#/media/File:Shrek_(2001_animated_feature_film).jpg', 'https://www.youtube.com/watch?v=CwXOrWvPBPk', 1);

INSERT INTO movie (title, genre, release_year, director, image_url, trailer_link, franchise_id)
VALUES ('Shrek 2', 'Comedy, Adventure, Animation', 2004, 'Andrew Adamson', 'https://upload.wikimedia.org/wikipedia/en/b/b9/Shrek_2_poster.jpg', 'https://www.youtube.com/watch?v=xBgSfhp5Fxo', 1);

INSERT INTO movie (title, genre, release_year, director, image_url, trailer_link, franchise_id)
VALUES ('Shrek The Third', 'Comedy, Adventure, Animation', 2007, 'Chris Miller', 'https://upload.wikimedia.org/wikipedia/en/2/22/Shrek_the_Third_%282007_animated_feature_film%29.jpg', 'https://www.youtube.com/watch?v=_MoIr7811Bs', 1);

INSERT INTO character (gender, image_url, name) VALUES ('male', 'https://static.wikia.nocookie.net/shrek/images/9/9b/GoodShrekImage.png/revision/latest/scale-to-width-down/1200?cb=20201220080410', 'Shrek');
INSERT INTO character (gender, image_url, name) VALUES ('male', 'https://static.wikia.nocookie.net/shrek/images/d/dc/DonkeyTransparent.png/revision/latest?cb=20171218193004', 'Donkey');

insert into movie_character(movie_id, character_id) values (1, 1)